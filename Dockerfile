FROM debian:9 AS build
RUN apt update && apt install -y wget gcc make libpcre3-dev zlib1g-dev

RUN wget https://github.com/openresty/luajit2/archive/v2.1-20220309.tar.gz && tar -zxvf v2.1-20220309.tar.gz
RUN wget https://github.com/vision5/ngx_devel_kit/archive/v0.3.1.tar.gz && tar -zxvf v0.3.1.tar.gz
RUN wget https://github.com/openresty/lua-nginx-module/archive/v0.10.21.tar.gz && tar -zxvf v0.10.21.tar.gz
RUN wget http://nginx.org/download/nginx-1.19.3.tar.gz && tar -zxvf nginx-1.19.3.tar.gz && cd nginx-1.19.3
RUN wget https://github.com/openresty/lua-resty-core/archive/v0.1.23.tar.gz && tar -zxvf v0.1.23.tar.gz
RUN wget https://github.com/openresty/lua-resty-lrucache/archive/v0.13.tar.gz && tar -zxvf v0.13.tar.gz
RUN cd luajit2-2.1-20220309 && make && make install
RUN cd nginx-1.19.3 && export LUAJIT_LIB=/usr/local/lib && export LUAJIT_INC=/usr/local/include/luajit-2.1 && \
	 ./configure --prefix=/opt/nginx --with-ld-opt="-Wl,-rpath,/usr/local/lib" --add-dynamic-module=/ngx_devel_kit-0.3.1 --add-dynamic-module=/lua-nginx-module-0.10.21 && make -j2 && make install

RUN cd lua-resty-core-0.1.23 && make install PREFIX=/opt/nginx
RUN cd lua-resty-lrucache-0.13 && make install PREFIX=/opt/nginx

FROM debian:9
WORKDIR /opt/nginx/sbin/
COPY --from=build /opt/nginx/ /opt/nginx/
CMD ["./nginx", "-g", "daemon off;"]


